*** Settings ***
Library     ${CURDIR}${/}..${/}Python${/}WinAppDriverLibrary.py

*** Keywords ***
Setup WinAppDriver
    [Documentation]     Tu khoa nay duoc tao, khoi dong va gan dooi tuong WinappDrive cho mot doi tuong python
    ...  Lop doi tuong can de thuc hien ham assign_winappdriver(self, winappdriver)
    ...  Co the su dung thong qua viec tao doi tuong WinAppDriverLibrary
    ...  ${object_to_assign}: doi duong de assign voi winappdriver
    ...  ${host_ip}  ip cua sut tren winappdriver chay
    ...  Vis du
    ...  Import Library    ConfigurationAssistant    WITH NAME    CA
    ...  Setup WinAppDriver    CA    ${ca_host_fqdn}
    [Arguments]     ${object_to_assign}    ${host_ip}    ${user}=${frodob_user}    ${password}=${frodob_password}
    Set Suite Variable    ${WinAppDriverForClass}    WinAppDriverFor${object_to_assign}
    Import Library     ${CURDIR}${/}..${/}Python${/}WinAppDriverLibrary.py    WITH NAME    ${WinAppDriverForClass}
    ${WinAppDriverLib} =    Get Library Instance    ${WinAppDriverForClass}
    Run Keyword    ${WinAppDriverForClass}.Setup WinApp Driver Host    ${host_ip}    ${user}    ${password}
#    Run Keyword    ${WinAppDriverForClass}.Stop WinApp Driver
    Run Keyword    ${WinAppDriverForClass}.Start WinApp Driver
    Wait Until Keyword Succeeds    3x    15s    Run Keyword    ${WinAppDriverForClass}.Connect WinApp Driver
    Run Keyword    ${object_to_assign}.Assign WinAppDriver    wad=${WinAppDriverLib}

Run before
    log to console      --------------Suite Setup

Run after
    log to console      Teardown------------------
