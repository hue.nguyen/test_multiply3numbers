*** Settings ***
Documentation   Call function test from code Python
# Library     AutoItLibrary
# Library     Library       Remote    http://172.26.0.69:8270
Library     ${CURDIR}${/}..${/}Python${/}ConfigurationAssistant.py
Resource    ${CURDIR}${/}..${/}Robot${/}winappdriver_keywords.robot
Test Setup      Run before
Test Teardown   Run after


*** Variables ***
# ${n1_fqdn}                    10.17.16.203
# ${admin_user}                 .\\Administrator
# ${admin_password}             Equitrac1
# ${frodob_user}                asia\\Administrator
# ${frodob_password}            Equitrac1

${n1_fqdn}                    172.26.0.140
${admin_user}                 .\\Administrator
${admin_password}             kofax@01
${frodob_user}                asia\\Administrator
${frodob_password}            kofax@01


*** Keywords ***
Application Start Winappdriver
    [Arguments]    ${sut_host_dnsname}=${as_host_dnsname}
#    Stop Winapp Driver
    Start Winapp Driver
    Connect Winapp Driver

Setup WinAppDriver and CA Libraries
    Log to console    --------Cai dat WinappDriver va CA---------------
    Import Library    ${CURDIR}${/}..${/}Python${/}ConfigurationAssistant.py    WITH NAME    CaN
    Setup WinAppDriver    CaN    ${n1_fqdn}    ${frodob_user}    ${frodob_password}
    

*** Test Cases ***
Run ConfigurationAssistant
    # AutoItLibrary.Run    C:/Program Files (x86)/Windows Application Driver/WinAppDriver.exe     #user default : 127.0.0.1:4723
    # setUpClass
    # test multiply3numbers
    Setup WinAppDriver and CA Libraries
    CaN.Start Ca
    CaN.Test Multiply3numbers
    CaN.Close App
    WinAppDriverForCaN.Stop Winapp Driver
    log to console      ---------------RUn tesstcase nay-------------------