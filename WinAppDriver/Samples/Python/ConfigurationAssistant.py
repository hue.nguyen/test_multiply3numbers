#******************************************************************************
#
# Copyright (c) 2016 Microsoft Corporation. All rights reserved.
#
# This code is licensed under the MIT License (MIT).
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# // LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
#******************************************************************************


import logging
from robot.api import logger
from time import sleep
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC


class ConfigurationAssistant:
    """ AutoStore Management class """

    def __init__(self):
        self.wad = None
        self.ca_win = None
        self.ca_xpath = None

    def assign_winappdriver(self, wad):
        """ assigns winappdriver session """
        self.wad = wad

    def start_ca(self, ca_path=r"C:\WINDOWS\system32\calc.exe"):
        """ start Caculator in UI mode """
        logging.info("starting CA UI")
        action = self.wad.actions
        action.reset_actions()
        action.key_down(Keys.COMMAND).send_keys("r").key_up(Keys.COMMAND)
        action.perform()
        run_xpath = ".//Window[@Name='Run']"
        self.wad.wait.until(EC.visibility_of_element_located((By.XPATH, run_xpath)))
        action.send_keys(ca_path+Keys.ENTER)
        action.perform()

    def test_multiply3numbers(self):
        self.wad.desktop.find_element_by_name("4").click()
        self.wad.desktop.find_element_by_name("6").click()
        self.wad.desktop.find_element_by_name("8").click()
        self.wad.desktop.find_element_by_name("Multiply").click()
        self.wad.desktop.find_element_by_name("9").click()
        self.wad.desktop.find_element_by_name("Equals").click()
        self.wad.desktop.find_element_by_name("Multiply").click()
        self.wad.desktop.find_element_by_name("7").click()
        self.wad.desktop.find_element_by_name("Equals").click()

        # for win 10
        # self.wad.desktop.find_element_by_xpath("Four").click()
        # self.driver.find_element_by_name("Four").click()
        # self.driver.find_element_by_xpath("//Button[@AutomationId='multiplyButton']").click()
        # self.driver.find_element_by_xpath("//Button[@AutomationId='num6Button']").click()
        # self.driver.find_element_by_xpath("//Button[@AutomationId='equalButton']").click()
        # self.driver.find_element_by_name("Multiply by").click()
        # self.driver.find_element_by_name("Six").click()
        # self.driver.find_element_by_xpath("//Button[@AutomationId='equalButton']").click()

    def close_app(self):
        self.wad.desktop.find_element_by_name("Close").click()


