"""
Power shell builder module
"""

import base64
import subprocess


class PowershellBuilder:
    """
    build a powershell script to run
    """

    def __init__(self, hostname, username, password):
        self.script = ""
        self.remote = (hostname, username, password)
        self.pssession = self.new_ps_session(
            hostname, username, password)

    def flush(self):
        """ encod script to run """
        self.script = ""
        return self

    def encode_script(self):
        """ encod script to run """
        return base64.b64encode(self.script.encode('utf-16-le')).decode('utf-8')

    @staticmethod
    def new_ps_session(hostname, username, password):
        """ create a powershell remote session"""
        ps_cred = r"""
        $pass = ConvertTo-SecureString -String "{}" -AsPlainText -Force
        $fdb = New-Object -typename System.Management.Automation.PSCredential -ArgumentList "{}", $pass
        $robotRemote = New-PSSession -ComputerName "{}" -Credential $fdb
        """.format(password, username, hostname)
        return ps_cred
    

    def put_files(self, src, dest):
        """ upload files to remote server """
        ps_copy = r"""
        try{{
            Copy-Item "{}" -ToSession $robotRemote "{}" -Recurse
        }}
        finally {{
            Remove-PSSession -Session $robotRemote
        }}
        """.format(src, dest)
        self.script = self.pssession + ps_copy

# lay lai file tu remote server
    def get_files(self, src, dest):
        """ retrieve files to remote server """
        ps_copy = r"""
        try{{
            Copy-Item -FromSession $robotRemote "{}"  "{}" -Recurse
        }}
        finally {{
            Remove-PSSession -Session $robotRemote
        }}
        """.format(dest, src)
        self.script = self.pssession + ps_copy


# run command remote khong phai dong session
    def run_remote_ps(self, commands):
        """ run remote commands don't have to close session """
        ps_remote_ps = r"""
        try{{
            Invoke-Command -Session $robotRemote -ErrorAction Stop -ScriptBlock {{
                {}
            }}
        }}
        finally {{
            Remove-PSSession -Session $robotRemote
        }}
        """.format(commands)
        self.script = self.pssession + ps_remote_ps


# run remote commands 
    def run_local_ps(self, commands):
        """ run remote commands """
        ps_local = r"""
        {}
        """.format(commands)
        self.script = ps_local


#  thuc hien run remote commands
    def execute(self):
        """ run remote commands """
        powershell = [
            'PowerShell',
            '-NonInteractive',
            '-ExecutionPolicy',
            'Unrestricted',
            '-EncodedCommand',
            self.encode_script()]
        proc = subprocess.run(" ".join(powershell), capture_output=True)
        print(self.script)
        print("powershell output returned: %s" % proc.stdout)
        if proc.returncode != 0:
            raise AssertionError(
                "powershell error: {} {}".format(proc.stdout, proc.stderr))
        return proc.stdout