"""
WinappDriverLibrary keywords and methods
"""
from appium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from powershellbuider import PowershellBuilder
import os
from robot.api import logger


class WinAppDriverLibrary:
    """WinappDriver Operations"""

    ROBOT_LIBRAYRY_SCOPE = 'TEST CASE'

    def __init__(self):
        self.powershell = None
        self.wait = None
        self.session = None
        self.desktop = None
        self.actions = None
        self.app = None
        self.ps_win = None
    
    """Ham setup  to use winappdriver for host"""
    def setup_winapp_driver_host(self, host, username, password):
        self.powershell = PowershellBuilder(
            host, username, password)
    

    # start winappDriver tren task scheduler
    def start_winapp_driver(self):
        """ start winapp driver via task scheduler """
        self._enable_sut_dev_mode() #goi ham enable windows developer mode to run winappdriver, phai bat developer mode thi moi chay dc winappdriver
        task_name = "robotTask"
        winapp_path = (
            "\"'C:\\Program Files (x86)\\Windows Application Driver"
            "\\WinAppDriver.exe' * 4723\""
            )
        commands = r"""
schtasks.exe /create /tn {0} /tr {1} /sc ONSTART /rl highest
Start-Sleep -Seconds 5
schtasks.exe /run /tn {2}
""".format(task_name, winapp_path, task_name)
        ps_script = self.powershell.flush()
        ps_script.run_remote_ps(commands)
        result_string = ps_script.execute()
        if result_string.find(b"successfully been created") == -1:
            raise Exception("Task: {0} already exists!".format(task_name))


    def stop_winapp_driver(self):
        """ stop winapp driver
        via task scheduler
        """
        commands = r"""
schtasks.exe /end /tn robotTask
Start-Sleep -Seconds 5
schtasks.exe /delete /tn robotTask /F
"""
        ps_script = self.powershell.flush()
        ps_script.run_remote_ps(commands)
        ps_script.execute()

    def disconnect_winapp_driver(self):
        """ disconnect winapp driver """
        if self.ps_win:
            self.ps_win.send_keys("exit"+Keys.ENTER)
        if self.session:
            self.session.quit()

    # enable windows developer mode to run winappdriver
    def _enable_sut_dev_mode(self):
        """ enable windows developer mode to run winappdriver """
        commands = r"""
$reg_key = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\AppModelUnlock";
$reg_lic = "AllowDevelopmentWithoutDevLicense";
$reg_trust = "AllowAllTrustedApps"
reg add $reg_key /t REG_DWORD /f /v $reg_trust /d '1'
reg add $reg_key /t REG_DWORD /f /v $reg_lic /d '1'
"""
        ps_script = self.powershell.flush()
        ps_script.run_remote_ps(commands)
        ps_script.execute()

    
    # CONNECT winapp driver 
    def connect_winapp_driver(self):
        """ connect winapp driver """
        session = webdriver.Remote(
            command_executor='http://%s:4723' % self.powershell.remote[0],
            desired_capabilities={"app": "Root"})
        self.session = session
        session.implicitly_wait(180)
        desktop = session.find_element_by_xpath("/Pane")
        desktop.send_keys(Keys.COMMAND+"d")
        self.desktop = desktop
        self.actions = ActionChains(session)
        self.wait = WebDriverWait(session, 300)

    
    def take_screenshot(self, testType):
        """ take a screen shot """
        filename = "{testType}-screenshot-{index}.png"
        path = self.gen_filename(filename, testType)
        self.desktop.screenshot(path)
        logger.info('</td></tr><tr><td colspan="3">'
                    '<a href="{src}"><img src="{src}" width="{width}px"></a>'
                    .format(src=path, width=400), html=True)

    
    @staticmethod
    def gen_filename(tpl, testType):
        """ generate a valid filename using a template """
        index = 1
        path = tpl.format(testType=testType, index=index)
        while os.path.exists(path):
            index += 1
            path = tpl.format(testType=testType, index=index)
        return path
        