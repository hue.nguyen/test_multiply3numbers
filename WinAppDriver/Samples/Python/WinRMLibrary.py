"""
Robot Framework library for Windows Remote Management, based on pywinrm.
"""
import base64
import time
from robot.api import logger
from robot.utils import ConnectionCache
import winrm
import xmltodict


class WinRMLibrary():
    """
    Robot Framework library for Windows Remote Management, based on pywinrm.

    == Enable Windows Remote Shell ==
    - [ http://support.microsoft.com/kb/555966 | KB-555966 ]
    - Execute on windows server:

     | winrm set winrm/config/client/auth @{Basic="true"}
     | winrm set winrm/config/service/auth @{Basic="true"}
     | winrm set winrm/config/service @{AllowUnencrypted="true"}

    == Dependence ==
    | pywinrm | https://pypi.python.org/pypi/pywinrm |
    | robot framework | http://robotframework.org |
    """

    ROBOT_LIBRARY_SCOPE = 'GLOBAL'

    def __init__(self, transport_type="ntlm", https=False, ssl_validate=True):
        """ Init method.
        *Args:*\n
        _transport_type_ - transport type one of 'plaintext', 'kerberos', 'ssl',
                            'ntlm' (default), 'credssp'
        _https_ - the winrm connection type such as https://
        _ssl_validate_ - if validate server ssl certificate at client side
        *Example:*\n
        | Library | WinRM  |  transport_type=plaintext |  ssl_validate=False  |  https=True |
        """
        self.kwargs = {
            "transport": transport_type,
            "server_cert_validation": "validate" if ssl_validate else "ignore",
            "service": "https" if https else "http"
        }
        self._session = None
        self._cache = ConnectionCache('No sessions created')

    def create_session(self, alias, hostname, login, password):
        """
        Create session with windows host.

        Does not support domain authentification.

        *Args:*\n
        _alias_ - robot framework alias to identify the session\n
        _hostname_ -  windows hostname (not IP)\n
        _login_ - windows local login\n
        _password_ - windows local password
        _transport_type_ - transport type one of 'plaintext', 'kerberos', 'ssl',
                            'ntlm' (default), 'credssp'

        *Returns:*\n
        Session index

        *Example:*\n
        | Create Session  |  server  |  windows-host |  Administrator  |  1234567890 |
        """

        logger.debug('Connecting using : hostname={0:s}, login={1:s}, '
                     'password={2:s} '.format(hostname, login, password))
        self._session = winrm.Session(hostname, (login, password), **self.kwargs)
        return self._cache.register(self._session, alias)

    def run_command(self, alias=None, command="", params=None):
        """
        Execute command on remote machine.
        """
        if params is not None:
            log_cmd = command + ' ' + ' '.join(params)
        else:
            log_cmd = command
        logger.debug('Run command on server with alias "{0:s}": {1:s} '
                     .format(alias, log_cmd))
        if alias:
            self._session = self._cache.switch(alias)
        session = self._session
        connected = False
        err = None
        for i in range(0, 10):
            try:
                shell_id = session.protocol.open_shell()
                command_id = session.protocol.run_command(shell_id, command, params)
                result = self._session.protocol.get_command_output(shell_id, command_id)
                resp = winrm.Response(result)
                session.protocol.cleanup_command(shell_id, command_id)
                session.protocol.close_shell(shell_id)
                connected = True
                break
            except Exception as ex:
                err = ex
                print("Retrying {} out of 10".format(i+1))
            time.sleep(30)

        if not connected:
            raise AssertionError(
                "fail to connect winrm service: {}".format(err))
        return resp

    def run_powershell(self, alias=None, script=""):
        """
        Run power shell script on remote machine.
        """
        logger.info('Run power shell script on server with alias "%s": %s ' % (alias, script))
        if not script:
            raise AssertionError("could not run empty powershell script")
        powershell = [
            'PowerShell',
            '-NoProfile',
            '-NonInteractive',
            '-ExecutionPolicy',
            'Unrestricted',
            '-EncodedCommand',
            _encode_script(script)]
        resp = self.run_command(alias, ' '.join(powershell))
        clixml = resp.std_err
        if clixml.startswith(b"#< CLIXML"):
            clixml = clixml.replace(b'#< CLIXML\r\n', b'')
        
        doc = xmltodict.parse(clixml)
        try:
            lines = [
                l.get('#text', '').replace('_x000D__x000A_', '')
                for l in doc.get('Objs', {}).get('S', {}) if l.get('@S') == 'Error']
            resp.std_err = ' '.join(lines)
        except:
            resp.std_err = "Unknown error"
            print("resp.std_err in raw dictionary format is %s\n" % doc)

        return resp

    def run_ps_file(self, alias=None, path="", log_output=False, * args):
        ps_file = open(path, "r", encoding="utf-8")
        ps_template = ps_file.read()
        ps_script = ps_template.format(*args)
        resp = self.run_powershell(alias, ps_script)
        if log_output:
            logger.info("CA log start")
            logger.info(resp.std_out)
            logger.info("CA log end")
        if (resp.status_code != 0):
            raise Exception(resp.std_err)
        return resp

    def delete_all_sessions(self):
        """ Removes all sessions with windows hosts"""
        self._cache.empty_cache()


def _encode_script(script):
    """ convert a powershell script to a single base64-encode command."""
    if not isinstance(script, str):
        raise AssertionError("could not encode not text_type")
    return base64.b64encode(script.encode('utf-16-le')).decode('utf-8')
